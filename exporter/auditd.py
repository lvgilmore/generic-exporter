#! /usr/bin/env python3
from datetime import datetime
from prometheus_client import Counter

from .common import common_labels, logger, metric_provider, recurring

LAST_AUDIT_CHECKPOINT = datetime(1970, 1, 1, 0, 0, 0)
AUDITD_SPECIFIC_LABELS = ["res", "exe", "syscall"]
AUDITD_EVENTS_COUNTER = Counter(
    name="auditd_syscall_event",
    documentation="number of syscall events caught by auditd",
    labelnames=[*common_labels, *AUDITD_SPECIFIC_LABELS])


def extract_labels_from_audit_lint(line: str) -> dict:
    labels = {}
    for pair in line.split(" "):
        if "=" not in pair:
            continue
        k, v = pair.split("=", 1)
        if k in AUDITD_SPECIFIC_LABELS:
            labels[k] = v
    return labels


@recurring(10)
@metric_provider
def count_audit_lines(*args, **kwargs) -> None:
    basical_labels = kwargs.get('labels', {})
    ignore_before = LAST_AUDIT_CHECKPOINT.timestamp()
    with open("/var/log/audit/audit.log", "r") as audit_file:
        for line in audit_file.readlines():
            line_timestamp = line.split(" ")[1].rstrip("msg=audit(").split(
                ":")[0]
            try:
                line_timestamp = float(line_timestamp)
            except ValueError as e:
                logger.info(
                    f"Failed to parse audit line timestamp, got exception {e}")
                continue
            if line_timestamp < ignore_before or line.split(
                    " ")[0] != "type=SYSCALL":
                continue

            addditional_labels = extract_labels_from_audit_lint(line)
            AUDITD_EVENTS_COUNTER.labels(
                basical_labels.update(addditional_labels)).inc()
