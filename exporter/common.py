#! /usr/bin/env python3
import os
import schedule
import sys
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format=
    '%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
    datefmt='%a, %d %b %Y %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stderr)])
common_labels = ["hostname", "os"]
logger = logging.getLogger(__name__)
_scheduled_methods = set()


def get_common_labels_with_values() -> dict:
    return {"hostname": os.uname().nodename, "os": os.uname().version}


def recurring(seconds: int):

    def decorator(fn):
        if fn.__repr__ not in _scheduled_methods:
            logger.info(
                f"adding function {fn.__repr__()} to schedule every {seconds}s"
            )
            schedule.every(seconds).seconds.do(fn)
            _scheduled_methods.add(fn.__repr__)
        return fn

    return decorator


def metric_provider(fn):

    def decorator(*args, **kwargs):
        return fn(*args, labels=get_common_labels_with_values())

    return decorator
