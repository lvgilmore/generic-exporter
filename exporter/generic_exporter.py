#! /usr/bin/env python3
from prometheus_client import start_http_server
from schedule import run_pending
from time import sleep

from exporter import netstat


def main():
    start_http_server(port=8083)
    while True:
        run_pending()
        sleep(5)


if __name__ == '__main__':
    main()
