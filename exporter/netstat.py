#! /usr/bin/env python3
from sys import stdout
from prometheus_client import Gauge
from subprocess import Popen, PIPE

from .common import common_labels, metric_provider, recurring, logger

SUM_RECV_GAUGE = Gauge(name="netstat_tcp4_recvq_sum",
                       documentation="sum of recv-q as captured by netstat",
                       labelnames=common_labels)


def get_total_recv_q() -> float:
    return float(
        Popen(
            "netstat -n -p tcp | awk '{print $2}' | grep '[0-9]' | "
            "paste -s -d + - | bc",
            shell=True,
            stdout=PIPE,
            stderr=PIPE).communicate()[0].decode())


@recurring(6)
@metric_provider
def sum_netstat_recvq(*args, **kwargs) -> None:
    logger.info(f"called netstat_recvq with args {kwargs}")
    SUM_RECV_GAUGE.labels(**kwargs.get('labels', {})).set(get_total_recv_q())
