import setuptools

setuptools.setup(name="exporter",
                 version="1.0.0",
                 packages=["exporter"],
                 entry_points={
                     'console_scripts':
                     ['expoter=exporter.generic_exporter:main']
                 })
